### 项目代码  
data_explore/数据探索  
FE_original_feature/原始特征数据处理  
FE_construction_feature/构造特征数据处理  
model_train/模型训练（Tree和LightGBM）  
model_test/模型测试  
wideanddeep_model/Wide and Deep模型训练和测试  
xgboost_model/xgboost模型训练

### 项目文档  
初步规划  
week2进展  
week3进展  
week4进展   
音乐推荐项目总结 
